import React from 'react';
import classes from './NavItem.module.css';
import { NavLink } from 'react-router-dom';

const navItem = (props) => (
    <NavLink className={classes.NavItem} activeClassName={classes.Active} to={props.url}>{props.children}</NavLink>
);

export default navItem;