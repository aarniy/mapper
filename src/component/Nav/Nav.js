import React from 'react';
import classes from './Nav.module.css';
import NavItem from './NavItem/NavItem';

import UserIcon from '../../asset/icons/user.svg';

const nav = (props) => {
    let navElement = null;
    if (props.isLoggedIn) {
        navElement = (
            <div className={classes.Nav}>
                <div className={classes.Filler}></div>
                <NavItem url='/map'>Add Location</NavItem>
                <div className={classes.Username}><img alt='user icon' src={UserIcon}></img>{props.username}</div>
            </div>
        );
    }
    return navElement;
};

export default nav;