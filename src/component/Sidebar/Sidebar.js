import React from 'react';
import classes from './Sidebar.module.css';
import ExitIcon from '../../asset/icons/exit.svg'; 
import MapIcon from '../../asset/icons/map.svg';
import ExploreIcon from '../../asset/icons/explore.svg';
import Logo from '../../asset/logo.svg';

import { NavLink } from 'react-router-dom';

const sidebar = (props) => {
    return (
        <div className={classes.Sidebar}>
                <NavLink className={classes.Logo} title="Home" to="/"><img src={Logo} alt="icon"></img></NavLink>
                <NavLink title="Map" to="/map"><img src={MapIcon} alt="icon"></img></NavLink>
                {/*<NavLink title="Randomizer" to="/random"><img src={ExploreIcon} alt="icon"></img></NavLink>*/}
                <NavLink title="Log Out" className={classes.Bottom} to="/logout"><img src={ExitIcon} alt="icon"></img></NavLink>
        </div>
    );
};

export default sidebar;