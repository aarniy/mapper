import React from 'react';

import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
// import LeafletMarker from './LeafletMarker/LeafletMarker';
import classes from './LeafletMap.module.css';
// separate file without modules for the leaflet map, because it's hard to import the modules otherwise
import './LeafletStyling.css';
import * as iconTypes from './LeafletMarker/LeafletIcons';
import CategoryButton from '../UI/CategoryButton/CategoryButton';

const leafletMap = (props) => {
    let tileLayer = (<TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />);

    const transformedLocations = props.locations.map((locdata) => {
        return (
            <Marker key={locdata.id} position={locdata.coords} icon={iconTypes[locdata.type]}>
                <Popup>
                    <p>{[locdata.note]}</p>
                    <a className={classes.Link} href={locdata.link} target='_blank' rel='noopener noreferrer'>Go to maps</a>
                    <CategoryButton className={classes.Button} val='trash' clicked={(event) => props.del(event, locdata.id)}>Delete</CategoryButton>
                </Popup>
            </Marker>
        );
    });
    return (
        <div className={classes.MapComponent}>
            <Map
                center={props.position} 
                zoom={props.zoom}
                className={classes.LeafletMap}>
                {tileLayer}
                {transformedLocations}
                {/*<LeafletMarker popupContent="Add things pls" position={props.position} icon={ iconTypes.reactIcon }/>*/}
            </Map>
        </div>
    );
};

export default leafletMap;