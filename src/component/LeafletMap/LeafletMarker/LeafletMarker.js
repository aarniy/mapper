import React from 'react';

import { Marker, Popup } from 'react-leaflet';

const marker = (props) => (
    <Marker position={props.position} icon={props.icon}>
        <Popup>
            <p>
                {props.popupContent}
            </p>
        </Popup>
    </Marker>
);

export default marker;