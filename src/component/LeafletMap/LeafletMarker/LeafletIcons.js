import L from 'leaflet';

const landscape = new L.Icon({
    iconUrl: require('../../../asset/marker_green.svg'),
    shadowUrl: require('../../../asset/marker_shadow.svg'),
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
    className: 'iconLandscape'
});

const cityscape = new L.Icon({
    iconUrl: require('../../../asset/marker_teal.svg'),
    shadowUrl: require('../../../asset/marker_shadow.svg'),
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
    className: 'iconCityscape'
});

const other = new L.Icon({
    iconUrl: require('../../../asset/marker.svg'),
    shadowUrl: require('../../../asset/marker_shadow.svg'),
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
    className: 'iconOther'
});

const building = new L.Icon({
    iconUrl: require('../../../asset/marker_orange.svg'),
    shadowUrl: require('../../../asset/marker_shadow.svg'),
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
    className: 'iconBuilding'
});

const car = new L.Icon({
    iconUrl: require('../../../asset/marker_red.svg'),
    shadowUrl: require('../../../asset/marker_shadow.svg'),
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
    className: 'iconCar'
});

const special = new L.Icon({
    iconUrl: require('../../../asset/marker_yellow.svg'),
    shadowUrl: require('../../../asset/marker_shadow.svg'),
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
    className: 'iconSpecial'
});

export { 
    landscape, 
    cityscape, 
    building,
    other,
    car,
    special
};