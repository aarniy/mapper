import React from 'react';
import classes from './SideContainer.module.css';

const sidebar = (props) => {
    return (
        <div className={classes.Sidebar}>
            {props.children}
        </div>
    );
};

export default sidebar;