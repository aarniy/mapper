import React, { Component } from 'react';
import classes from './PostForm.module.css';
import Input from '../UI/Input/Input';
import Button from '../UI/Button/Button';
import CategoryButton from '../UI/CategoryButton/CategoryButton';
// import LoadingAnim from '../UI/LoadingAnim/LoadingAnim';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/';
import Message from '../UI/Message/Message';
import * as categories from '../LeafletMap/LeafletMarker/LeafletIcons';

class postForm extends Component {
    state = {
        location: null,
        category: 'landscape',
        note: null,
        addable: false,
        error: null,
        coordinates: []
    }

    submitClickedHandler = (event) => {
        event.preventDefault();
        try {
            let url = this.state.location.split("@");
            let at = url[1].split("z");
            let parts = at[0].split(",");
            if (parts[0] > 0 && parts[1] > 0 && parts[0] < 90 && parts[1] < 180) {
                const locationData = {
                    link: this.state.location,
                    type: this.state.category,
                    note: this.state.note,
                    coords: [parseFloat(parts[0]), parseFloat(parts[1])]
                };
                this.props.onSubmitForm(locationData, this.props.token, this.props.userId);
            }
        } catch (error) {
            this.setState({error: error});
        }
    }

    linkHandler = (event) => {
        this.setState({
            ...this.state,
            location: event.target.value
        }, () => {
            //this.validateLink(event.target.value);
        });
    }

    noteHandler = (event) => {
        this.setState({
            ...this.state,
            note: event.target.value
        });
    }

    categoryClickedHandler = (event) => {
        event.preventDefault();
        this.setState({
            ...this.state,
            category: event.target.value
        });
    }

    render () {
        let transformedCategories = Object.keys(categories);
        const categoryButtons = transformedCategories.map((cat) => {
            return <CategoryButton key={cat} clicked={this.categoryClickedHandler} active={this.state.category} val={cat}>{cat}</CategoryButton>
        });

        let confirmer = null;
        if (this.props.error) {
            confirmer = <p style={{color: 'red'}}>{this.props.error.message}</p>;
        }else if (this.props.justAdded) {
            confirmer = <Message>Location added</Message>;
        }
        return (
            <form className={classes.PostForm} onSubmit={this.submitClickedHandler}>
                <Input changed={this.linkHandler} placeholder='Street View -Link'></Input>
                <Input changed={this.noteHandler} type='textarea' placeholder='Notes'></Input>
                <div className={classes.Selector}>
                    {categoryButtons}
                </div>
                
                {confirmer}
                <Button>Add Location</Button>
            </form>
        );
    };
    
};

const mapStateToProps = state => {
    return {
        userId: state.auth.id,
        token: state.auth.token,
        error: state.mapView.error,
        justAdded: state.mapView.addSuccess
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmitForm: (locdata, token, uid) => dispatch(actions.addLocation(locdata, token, uid))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(postForm);