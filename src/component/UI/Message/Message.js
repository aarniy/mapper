import React from 'react';
import classes from './Message.module.css';

const message = (props) => (
    <div className={classes.Message}>{props.children}</div>
);

export default message;