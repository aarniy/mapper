import React from 'react';
import classes from './LoadingAnim.module.css';

const loadingAnim = () => (
    <div className={classes.Loader}>Loading...</div>
);

export default loadingAnim;