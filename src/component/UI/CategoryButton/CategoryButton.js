import React from 'react';
import classes from './CategoryButton.module.css';

export const CategoryButton = (props) => {
    let activeStyle = classes.Button;
    if (props.active === props.val) activeStyle = classes.ButtonActive;
    return (
        <button onClick={props.clicked} className={activeStyle} value={props.val}>{props.children}</button>
    );
    
};

export default CategoryButton;