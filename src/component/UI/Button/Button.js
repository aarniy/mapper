import React from 'react';
import classes from './Button.module.css';

const button = (props) => {
    let but = <button className={classes.Button}>{props.children}</button> ;
    if (props.clickable === false) {
        but = <button className={classes.ButtonDisabled}>{props.children}</button>
    }
    return (
        <div className={classes.ButtonDiv}>
            {but}
        </div>
    );
}

export default button;