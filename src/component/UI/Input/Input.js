import React from 'react';
import classes from './Input.module.css';

const input = (props) => {
    return (
        <div className={classes.Input}>
            <label className={classes.Label}>{props.label}</label>
            <input 
                type={props.type}
                placeholder={props.placeholder}
                className={classes.InputElement} 
                {...props.elementConfig} 
                value={props.value} 
                onChange={props.changed}/>
        </div>
    );
};

export default input;