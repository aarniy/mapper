import React from 'react';

import Auth from './container/Auth/Auth';
import MapView from './container/MapView/MapView';
import RandomView from './container/RandomView/RandomView';
import Logout from './component/Logout/Logout';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

function App(props) {
  let routes = (
    <Switch>
      <Route path='/login' component={() => <Auth signup={false} />}></Route>
      <Route path='/signup' component={() => <Auth signup={true} />}></Route>
      <Redirect to='/login' />
    </Switch>
  );

  if (props.isLoggedIn) {
    routes = (
      <Switch>
        <Route path='/map' component={MapView}></Route>
        <Route path='/random' component={RandomView}></Route>
        <Route path='/logout' component={Logout}></Route>
        <Redirect to='/map' />
      </Switch>
    );
  }

  return (
    <main className="Root">
      <BrowserRouter>
          {routes}
      </BrowserRouter>
    </main>
  );
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.auth.token !== null
  };
};

export default connect(mapStateToProps)(App);
