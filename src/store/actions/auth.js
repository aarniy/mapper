import axios from 'axios';
import * as actionTypes from './actionTypes';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authSuccess = (username, token, userId, refreshToken) => {
    //setTimeout(refreshLogin(refreshToken), 3000);
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token,
        refreshToken: refreshToken,
        id: userId,
        username: username
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};

export const logout = () => {
    return {
        type: actionTypes.AUTH_LOGOUT
    };
};

export const login = (email, password) => {
    return dispatch => {
        dispatch(authStart());
        const loginData = {
            email: email,
            password: password,
            returnSecureToken: true
        };
        axios.post('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDMwC-X4fmm4hWJzexBbozX1DaQqjjoHWo', loginData)
            .then(response => {
                dispatch(checkAuthTimeout(response.data.expiresIn));
                dispatch(authSuccess(response.data.displayName, response.data.idToken, response.data.localId, response.data.refreshToken));
            })
            .catch(err => {
                dispatch(authFail(err.response.data.error));
            });
    };
};

export const signup = (email, displayName, password) => {
    return dispatch => {
        dispatch(authStart());
        const signupData = {
            email: email,
            displayName: displayName,
            password: password,
            returnSecureToken: true
        };
        axios.post('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDMwC-X4fmm4hWJzexBbozX1DaQqjjoHWo', signupData)
            .then(response => {
                dispatch(checkAuthTimeout(response.data.expiresIn));
                dispatch(authSuccess(response.data.idToken, response.data.localId, response.data.refreshToken));
            })
            .catch(err => {
                dispatch(authFail(err.response.data.error));
            });
    };
};

// DOES NOT WORK AS OF NOW
export const refreshLogin = (refreshToken) => {
    return dispatch => {
        axios.post('https://securetoken.googleapis.com/v1/token?key=AIzaSyDMwC-X4fmm4hWJzexBbozX1DaQqjjoHWo', refreshToken)
            .then(response => {
                console.log(response);
                dispatch(authSuccess(response.data.displayName, response.data.idToken, response.data.localId, response.data.refreshToken));
            })
            .catch(err => {
                dispatch(authFail(err.response.data.error));
            });
    };
};

// temporary
export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime * 1000);
    };
};