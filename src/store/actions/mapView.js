import * as actionTypes from './actionTypes';
import axios from '../../axios-orders';

export const fetchLocationsSuccess = (locations) => {
    return {
        type: actionTypes.FETCH_LOCATIONS_SUCCESS,
        locations: locations
    };
};

export const fetchLocationsFail = (error) => {
    return {
        type: actionTypes.FETCH_LOCATIONS_FAIL,
        error: error
    };
};

export const fetchLocationsInit = () => {
    return {
        type: actionTypes.FETCH_LOCATIONS_INIT
    };
};

export const fetchLocations = (token, uid) => {
    return dispatch => {
        axios.get('/places/' + uid + '.json?auth=' + token)
            .then((response) => {
                const locations = [];
                for (let key in response.data) {
                    locations.push({
                        ...response.data[key],
                        id: key
                    });
                }
                dispatch(fetchLocationsSuccess(locations));
            })
            .catch((error) => {
                dispatch(fetchLocationsFail(error))
            });
    };
};

export const addLocation = (location, token, uid) => {
    return dispatch => {
        dispatch(addLocationInit());
        axios.post('/places/' + uid + '.json?auth=' + token, location)
            .then((response) => {
                dispatch(addLocationSuccess(response.data.name, location));
                // dispatch(fetchLocations(token, uid));
            })
            .catch((error) => {
                console.log(error);
            });
    };
}

export const addLocationInit = () => {
    return {
        type: actionTypes.ADD_LOCATION_INIT
    }
}

export const addLocationSuccess = (locId, location) => {
    const newLocation = {
        link: location.link,
        type: location.type,
        id: locId,
        note: location.note,
        coords: location.coords
    };
    return {
        type: actionTypes.ADD_LOCATION_SUCCESS,
        location: newLocation
    }
}

export const removeLocation = (locationId, token, uid) => {
    return dispatch => {
        axios.delete('/places/' + uid + '/' + locationId +'.json?auth=' + token)
            .then((response) => {
                dispatch(fetchLocations(token, uid));
            })
            .catch((error) => {
                dispatch(removeLocationFail(error));
            });
    };
}

export const removeLocationSuccess = (location) => {
    return {
        type: actionTypes.REMOVE_LOCATION_SUCCESS,
        location: location
    }
}

export const removeLocationFail = (error) => {
    return {
        type: actionTypes.REMOVE_LOCATION_FAIL,
        error: error
    }
}