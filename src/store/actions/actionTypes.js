// fetching locations
export const FETCH_LOCATIONS_SUCCESS = 'FETCH_LOCATIONS_SUCCESS';
export const FETCH_LOCATIONS_FAIL = 'FETCH_LOCATIONS_FAILED';
export const FETCH_LOCATIONS_INIT = 'FETCH_LOCATIONS_INIT';

// Auth
export const AUTH_START = 'AUTH_START';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAIL = 'AUTH_FAIL';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';

// Adding locations
export const ADD_LOCATION_INIT = 'ADD_LOCATION_INIT';
export const ADD_LOCATION_SUCCESS = 'ADD_LOCATION_SUCCESS';
export const ADD_LOCATION_FAIL = 'ADD_LOCATION_SUCCESS';
export const UPDATE_LOCATION_STORE = 'UPDATE_LOCATION_STORE';

// Removing locations
export const REMOVE_LOCATION = 'REMOVE_LOCATION';
export const REMOVE_LOCATION_SUCCESS = 'REMOVE_LOCATION_SUCCESS';
export const REMOVE_LOCATION_FAIL = 'REMOVE_LOCATION_FAIL';