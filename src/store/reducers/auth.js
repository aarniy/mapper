import * as actionTypes from '../actions/actionTypes';

const initialState = {
    userName: null,
    token: null,
    refreshToken: null,
    id: null,
    error: null,
    loading: false
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.AUTH_START: 
            return { ...state,
                error: null, 
                loading: true
            };
        case actionTypes.AUTH_SUCCESS: 
            return { ...state,
                token: action.token,
                refreshToken: action.refreshToken,
                id: action.id,
                error: null,
                loading: false,
                userName: action.username
            };
        case actionTypes.AUTH_FAIL: 
            return { ...state,
                error: action.error,
                loading: false
            };
        case actionTypes.AUTH_LOGOUT: 
            return { ...state,
                token: null, 
                id: null, 
                refreshToken: null 
            };
        default: return state;
    }
};

export default reducer;