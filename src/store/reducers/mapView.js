import * as actionTypes from '../actions/actionTypes';

const initialState = {
    locations: null,
    loading: true,
    error: null,
    addSuccess: false,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_LOCATIONS_SUCCESS:
            // console.log(action.locations);
            return {
                ...state,
                loading: false,
                locations: action.locations,
                error: null
            };
        case actionTypes.FETCH_LOCATIONS_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false
            };
        case actionTypes.ADD_LOCATION_INIT: 
            return {
                ...state,
                addSuccess: false,
            };
        case actionTypes.ADD_LOCATION_SUCCESS: 
            return {
                ...state,
                loading: false,
                addSuccess: true,
                locations: [
                    ...state.locations,
                    action.location
                ]
            };
        case actionTypes.ADD_LOCATION_FAIL: 
            return {
                ...state,
                loading: false
            };
        case actionTypes.REMOVE_LOCATION_FAIL: 
            return {
                ...state,
                error: action.error
            };
        default:
            return state;

    }
};

export default reducer;