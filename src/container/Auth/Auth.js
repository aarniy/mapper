import React, { Component } from 'react';
import { connect } from 'react-redux';

import classes from './Auth.module.css';
import LoadingAnim from '../../component/UI/LoadingAnim/LoadingAnim';
import * as actions from '../../store/actions/';
import Input from '../../component/UI/Input/Input';
import Button from '../../component/UI/Button/Button';
import { Link } from 'react-router-dom';

class Auth extends Component {
    state = {
        isSignup: this.props.signup,
        username: "based fella",
        controls: {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Email Address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            pw: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            }
        }
    }

    checkValidity(value, rules) {
        let isValid = true;
        
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
        }

        return isValid;
    }

    inputChangeHandler = (event, controlName) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            }
        };
        this.setState({controls: updatedControls});
    }

    authStartHandler = (event) => {
        event.preventDefault();
        if (this.props.isSignup) this.props.onSignup(this.state.controls.email.value, this.state.username.value, this.state.controls.pw.value);
        else this.props.onLogin(this.state.controls.email.value, this.state.controls.pw.value);
    }

    switchAuthModeHandler = () => {
        this.setState(prevState => {
            return {isSignup: !prevState.isSignup};
        })
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id:key,
                config: this.state.controls[key]
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangeHandler(event, formElement.id)}/>
            
        ));

        let auth = null;
        if (this.props.loading) {
            auth = <LoadingAnim />
        }

        let error = null;
        if (this.props.error) {
            error = (
                <p className={classes.Error}>{this.props.error.message.replace(/_/g, ' ').toLowerCase()}</p>
            );
        }

        auth = (
            <div className={classes.Auth}>
                <div className={classes.Tabs}>
                    <Link className={!this.state.isSignup ? classes.Tab : classes.TabInact} to='/login'>Log In</Link>
                    <Link className={this.state.isSignup ? classes.Tab : classes.TabInact} to='/signup'>Sign Up</Link>
                </div>
                <form onSubmit={this.authStartHandler} className={classes.Form}>
                    {form}
                    {/*<Button clickable={false}>{!this.state.isSignup ? 'Log In' : 'Get trolled lmao you cant sign up gottem'}</Button>*/}
                    {!this.state.isSignup ? <Button>Log In</Button> : <Button clickable={false}>you can't sign up lmao gottem</Button>}
                    {error}
                </form>
            </div>
        );

        return (
            <div className={classes.AuthWrapper}>
                {auth}
            </div>
            );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        //onSignup: (email, pw, username) => dispatch(actions.signup(email, pw, username)),
        onLogin: (email, pw) => dispatch(actions.login(email, pw))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Auth);