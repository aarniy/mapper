import React, { Component } from 'react';
import { connect } from 'react-redux';

import LeafletMap from '../../component/LeafletMap/LeafletMap';
import PostForm from '../../component/PostForm/PostForm';
import LoadingAnim from '../../component/UI/LoadingAnim/LoadingAnim';
import * as actions from '../../store/actions/';
import SideContainer from '../../component/SideContainer/SideContainer';
import Sidebar from '../../component/Sidebar/Sidebar';
import Nav from '../../component/Nav/Nav';

class MapView extends Component {
    state ={
        startLatitude: 37.0038827,
        startLongitude: 138.3970362,
        startZoom: 5
    }

    componentDidMount() {
        this.props.onFetchLocations(this.props.token, this.props.userId);
    }

    locationDeleteHandler = (e, id) => {
        this.props.onRemoveLocation(id, this.props.token, this.props.userId);
    }

    render() {
        let map = <LoadingAnim />
        if (!this.props.loading && !this.props.error) {
            map = <LeafletMap
                position={[this.state.startLatitude, this.state.startLongitude]} 
                zoom={this.state.startZoom} 
                locations={this.props.locations}
                darkmode={false}
                del={this.locationDeleteHandler}/>;
        } else if (!this.props.loading && this.props.error) {
            map = <div></div>;
        }

        return (
            <div>
                {/*<Nav isLoggedIn={this.props.isLoggedIn} username={this.props.userName} />*/}
                <Sidebar />
                <SideContainer>
                    
                    <PostForm />
                </SideContainer>
                {map}
            </div>
            );
    }
}

const mapStateToProps = state => {
    return {
        userName: state.auth.userName,
        locations: state.mapView.locations,
        error: state.mapView.error,
        loading: state.mapView.loading,
        token: state.auth.token,
        userId: state.auth.id,
        isLoggedIn: state.auth.token !== null
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchLocations: (token, uid) => dispatch(actions.fetchLocations(token, uid)),
        onRemoveLocation: (id, token, uid) => dispatch(actions.removeLocation(id, token, uid))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MapView);