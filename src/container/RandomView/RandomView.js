import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/';
import Sidebar from '../../component/Sidebar/Sidebar';
import SideContainer from '../../component/SideContainer/SideContainer';
import Nav from '../../component/Nav/Nav';

class RandomView extends Component {
    state = {
        current: null,
        prevLocations: null
    }

    render () {
        return (
            <div>
                <Nav isLoggedIn={this.props.isLoggedIn}/>
                <Sidebar />
                <SideContainer>
                    
                        prev location || next location<br></br>
                        save view
                </SideContainer>
                <div>
                    sidebruh
                </div>
            </div>
        );
    }
}

const mapStateToProps = props => {
    return {
        isLoggedIn: props.auth.token !== null
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSaveCurrent: (locdata, token, uid) => dispatch(actions.addLocation(locdata, token, uid))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RandomView);