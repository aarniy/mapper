import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://mapbro-9337e.firebaseio.com/'
});

export default instance;